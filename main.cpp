#include <iostream>

using namespace std;
//NEW FILE
// Address class
class address{
public:
    string st, city, id;
};

//LIST
// Faculty Linked list

class faculty{
public:
    string name, id;
    friend ostream &operator <<(ostream &, const faculty&);
    friend istream &operator >>(istream &, faculty&);
};


istream &operator >>(istream &in, faculty &x){
    in >> x.name >> x.id;
    return in;
}


ostream &operator <<(ostream &out, const faculty &x){
    out << x.name << ", " << x.id << endl;
    return out;
}

struct Nodef{
    faculty data;
    Nodef *link;
};

Nodef *firstf = NULL;
Nodef *endf = NULL;

void insertf(faculty x){
    Nodef *p;
    p = new Nodef;
    p->data = x;
    p->link = NULL;
    if(firstf == NULL){
        firstf = p;
        endf = p;
        p = NULL;
    }
    else{
        endf->link = p;
        endf = p;
    }
}



void print_f(Nodef *first){
    if(first==NULL) return;
    Nodef *p;
    p = first;
    while(p != NULL){
        cout << p->data;
        p = p->link;
    }
}





// Student Linked List
class student{
    string name, family, id;
    faculty f;
    address ad;
public:
    friend ostream &operator <<(ostream &, const student&);
    friend istream &operator >>(istream &, student&);
};


istream &operator >>(istream &in, student &x){
    in >> x.name >> x.family >> x.id;
    in >> x.f;
    in >> x.ad.city >> x.ad.st >> x.ad.id;
    return in;
}


ostream &operator <<(ostream &out, const student &x){
    out << x.name << ' ' << x.family << " - " << x.id << endl << x.f << x.ad.city << ", " << x.ad.st << ", " << x.ad.id << endl;
    return out;
}

struct Nodes{
    student data;
    Nodes *link;
};

Nodes *firsts = NULL;
Nodes *end_s = NULL;

void inserts(student x){
    Nodes *p;
    p = new Nodes;
    p->data = x;
    p->link = NULL;
    if(firsts == NULL){
        firsts = p;
        end_s = p;
        p = NULL;
    }
    else{
        end_s->link = p;
        end_s = p;
    }
}

void print_s(Nodes *first){
    if(first==NULL) return;
    Nodes *p;
    p = first;
    while(p != NULL){
        cout << p->data;
        p = p->link;
    }
}








//Course class
class course {
    string name, id, teacher;
    int vahed;
};

struct Nodec{
    course data;
    Nodec *link;
};









//Teacher class
class teacher{
    string name, family, id;
    faculty f;
};

struct Nodet{
    teacher data;
    Nodet *link;
};









int main()
{
    cout << "Hello World!" << endl;

    student x;
    cin >>x;
    inserts(x);
//    cin >>x;
//    inserts(x);
//    cin >>x;
//    inserts(x);

    cout << endl;
    print_s(firsts);


    return 0;
}
